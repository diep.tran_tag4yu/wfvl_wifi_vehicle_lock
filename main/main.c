/* Captive Portal Example

    This example code is in the Public Domain (or CC0 licensed, at your option.)

    Unless required by applicable law or agreed to in writing, this
    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
    CONDITIONS OF ANY KIND, either express or implied.
*/

#include <sys/param.h>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"

#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_netif.h"
#include "lwip/inet.h"

#include "driver/gpio.h"

#include "esp_http_server.h"
#include "dns_server.h"

uint8_t ESP_SSID[32] = "Tag4Yu WFVL";
#define MAX_STA_CONN 4

#define ACTION_PIN 4

#define ERROR_INVALID_PIN 1
#define ERROR_WRONG_PIN 2
#define ERROR_PIN_NOMATCH 3
#define ERROR_INVALID_SSID 4
#define ERROR_INVALID_N_PIN 5

#define NOTI_INVALID_PIN        "PIN has not been entered"
#define NOTI_INVALID_PIN_VI     "Chưa nhập mã PIN"
#define NOTI_INVALID_SSID       "No personal hotspot name entered"
#define NOTI_INVALID_SSID_VI    "Chưa nhập tên điểm truy cập cá nhân"
#define NOTI_INVALID_N_PIN      "New PIN has not been entered"
#define NOTI_INVALID_N_PIN_VI   "Chưa nhập mã PIN mới"
#define NOTI_PIN_NOMATCH        "New PIN does not match"
#define NOTI_PIN_NOMATCH_VI     "Mã PIN mới không khớp"

extern const char root_start[] asm("_binary_root_html_start");
extern const char root_end[] asm("_binary_root_html_end");

extern const char wflanding_start[] asm("_binary_wflanding_html_start");
extern const char wflanding_end[] asm("_binary_wflanding_html_end");

extern const char wf_start[] asm("_binary_changewf_html_start");
extern const char wf_end[] asm("_binary_changewf_html_end");

extern const char pinlanding_start[] asm("_binary_pinlanding_html_start");
extern const char pinlanding_end[] asm("_binary_pinlanding_html_end");

extern const char pin_start[] asm("_binary_changepin_html_start");
extern const char pin_end[] asm("_binary_changepin_html_end");

extern const char rootvi_start[] asm("_binary_rootvi_html_start");
extern const char rootvi_end[] asm("_binary_rootvi_html_end");

uint8_t pinNumber[5] = "0000";
bool systemActive = false, authPin = false, smartSensor = false, haveChange = false, softApOn = true, langVI = false;
uint8_t n_countdown = 120, s_countdown[4], n_count_wrong_pin = 0, notify_wrong[100];
uint8_t errorIndex = 0;

bool readNVS();
bool writeNVS(uint8_t key);

static const char *TAG = "example";

wifi_config_t sta_config = {
    .sta = {
        .ssid = "",
        .password = "",
        /* Setting a password implies station will connect to all security modes including WEP/WPA.
         * However these modes are deprecated and not advisable to be used. Incase your Access point
         * doesn't support WPA2, these mode can be enabled by commenting below line */
        .threshold.authmode = WIFI_AUTH_WPA2_PSK,
    },
};

wifi_config_t ap_config = {
    .ap = {
        // .ssid = ESP_SSID,
        // .ssid_len = strlen((const char *)ESP_SSID),
        // .password = ESP_PASS,
        .channel = 1,
        .max_connection = MAX_STA_CONN,
        .authmode = WIFI_AUTH_OPEN},
};

static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED)
    {
        if (smartSensor)
        {
            esp_wifi_disconnect();
        }
        wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
        ESP_LOGI(TAG, "station " MACSTR " join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
    else if (event_id == WIFI_EVENT_AP_STADISCONNECTED)
    {
        authPin = false;
        if (!systemActive && smartSensor)
        {
            esp_wifi_connect();
        }
        if (haveChange)
        {
            esp_restart();
        }
        wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
        ESP_LOGI(TAG, "station " MACSTR " leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
    else if (event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (!systemActive)
        {
            esp_wifi_connect();
            ESP_LOGI(TAG, "retry to connect to the AP");
        }
        ESP_LOGI(TAG, "connect to the AP fail");
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED && !systemActive)
    {
        ESP_LOGI(TAG, "Success connect to the AP");
        systemActive = true;
    }
}

static void wifi_init_softap(void)
{
    if (strlen((char *)sta_config.sta.ssid) > 0)
    {
        ESP_LOGI(TAG, "Smart Sensor!");
        esp_netif_create_default_wifi_sta();
    }
    // Config DHCP to open pop-up on Android
    esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap();
    esp_netif_ip_info_t ip_info;
    IP4_ADDR(&ip_info.ip, 124, 213, 16, 29);
    IP4_ADDR(&ip_info.gw, 124, 213, 16, 29);
    IP4_ADDR(&ip_info.netmask, 255, 0, 0, 0);
    esp_netif_dhcps_stop(ap_netif);
    esp_netif_set_ip_info(ap_netif, &ip_info);
    esp_netif_dhcps_start(ap_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));

    strcpy((char *)ap_config.ap.ssid, (char *)ESP_SSID);
    ap_config.ap.ssid_len = strlen((char *)ESP_SSID);

    if (strlen((char *)sta_config.sta.ssid) > 0)
    {
        ESP_LOGI(TAG, "ESP Mode APSTA");
        if (strlen((char *)sta_config.sta.password) == 0)
        {
            sta_config.sta.threshold.authmode = WIFI_AUTH_OPEN;
        }
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
    }
    else
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    char ip_addr[16];
    inet_ntoa_r(ip_info.ip.addr, ip_addr, 16);
    ESP_LOGI(TAG, "Set up softAP with IP: %s", ip_addr);

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:'%s'",
             ESP_SSID);
}

void wifi_init_sta(void)
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    if (strlen((char *)sta_config.sta.password) == 0)
    {
        sta_config.sta.threshold.authmode = WIFI_AUTH_OPEN;
    }
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "wifi_init_sta finished.");
}

// HTTP GET Handler
static esp_err_t root_get_handler(httpd_req_t *req)
{
    char *cChar = strstr((const char *)root_start, "{C}");
    char *eChar = strstr((const char *)root_start, "{E}");
    char *bChar = strstr((const char *)root_start, "{B}");
    ESP_LOGI(TAG, "Serve root");
    httpd_resp_set_type(req, "text/html");

    httpd_resp_send_chunk(req, root_start, cChar - root_start);
    httpd_resp_sendstr_chunk(req, (char *)s_countdown);
    httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
    if (errorIndex == ERROR_INVALID_PIN)
    {
        httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN);
        errorIndex = 0;
    }
    if (errorIndex == ERROR_WRONG_PIN)
    {
        httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
        errorIndex = 0;
    }
    httpd_resp_send_chunk(req, eChar + 3, bChar - (eChar + 3));
    if (!systemActive)
    {
        httpd_resp_sendstr_chunk(req, "UNLOCK");
    }
    else
    {
        httpd_resp_sendstr_chunk(req, "LOCK");
    }
    httpd_resp_send_chunk(req, bChar + 3, root_end - (bChar + 3));
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t root = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = root_get_handler};

static esp_err_t root_vi_get_handler(httpd_req_t *req)
{
    langVI = true;
    char *cChar = strstr((const char *)rootvi_start, "{C}");
    char *eChar = strstr((const char *)rootvi_start, "{E}");
    char *bChar = strstr((const char *)rootvi_start, "{B}");
    ESP_LOGI(TAG, "Serve rootvi");
    httpd_resp_set_type(req, "text/html");

    httpd_resp_send_chunk(req, rootvi_start, cChar - rootvi_start);
    httpd_resp_sendstr_chunk(req, (char *)s_countdown);
    httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
    if (errorIndex == ERROR_INVALID_PIN)
    {
        httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN_VI);
        errorIndex = 0;
    }
    if (errorIndex == ERROR_WRONG_PIN)
    {
        httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
        errorIndex = 0;
    }
    httpd_resp_send_chunk(req, eChar + 3, bChar - (eChar + 3));
    if (!systemActive)
    {
        httpd_resp_sendstr_chunk(req, "MỞ KHÓA");
    }
    else
    {
        httpd_resp_sendstr_chunk(req, "KHÓA");
    }
    httpd_resp_send_chunk(req, bChar + 3, rootvi_end - (bChar + 3));
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t root_vi = {
    .uri = "/vi",
    .method = HTTP_GET,
    .handler = root_vi_get_handler};

static esp_err_t unlock_handler(httpd_req_t *req)
{
    uint8_t buffer[15];
    char _pinInput[5];
    httpd_req_recv(req, (char *)buffer, 15);
    if (httpd_query_key_value((char *)buffer, "pin", (char *)_pinInput, 5) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input PIN");
    }
    if (!langVI)
    {
        if (strcmp((char *)_pinInput, (char *)pinNumber) == 0)
        {
            ESP_LOGI(TAG, "PIN is right");
            if (!systemActive)
                systemActive = true;
            else
                systemActive = false;
            n_count_wrong_pin = 0;
        }
        else
        {
            if (strlen((char *)_pinInput) == 0)
                errorIndex = ERROR_INVALID_PIN;
            else
            {
                errorIndex = ERROR_WRONG_PIN;
                n_count_wrong_pin++;

                if (n_count_wrong_pin == 1)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d time", n_count_wrong_pin);
                else if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d times", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Enter the wrong PIN more than 6 times<br>Disconnect after <span id='e'></span> seconds");
                }
            }
        }

        root_get_handler(req);
    }
    else
    {
        if (strcmp((char *)_pinInput, (char *)pinNumber) == 0)
        {
            ESP_LOGI(TAG, "PIN is right");
            if (!systemActive)
                systemActive = true;
            else
                systemActive = false;
            n_count_wrong_pin = 0;
        }
        else
        {
            if (strlen((char *)_pinInput) == 0)
                errorIndex = ERROR_INVALID_PIN;
            else
            {
                errorIndex = ERROR_WRONG_PIN;
                n_count_wrong_pin++;

                if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Nhập sai mã PIN %d lần", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Nhập sai mã PIN quá 6 lần<br>Ngắt kết nối sau <span id = 'e'></span> giây");
                }
            }
        }
        root_vi_get_handler(req);
    }

    return ESP_OK;
}

static const httpd_uri_t unlock = {
    .uri = "/unlock",
    .method = HTTP_POST,
    .handler = unlock_handler};

static esp_err_t lang_en_handler(httpd_req_t *req)
{
    langVI = false;
    root_get_handler(req);

    return ESP_OK;
}

static const httpd_uri_t lang_en = {
    .uri = "/en",
    .method = HTTP_GET,
    .handler = lang_en_handler};

// Landingpage change Wi-Fi - EN
static esp_err_t WFLanding_handler(httpd_req_t *req)
{
    // const uint32_t wflanding_len = wflanding_end - wflanding_start;
    char *cChar = strstr((const char *)wflanding_start, "{C}");
    char *eChar = strstr((const char *)wflanding_start, "{E}");
    char *aChar = strstr((const char *)wflanding_start, "{A}");
    char *vChar = strstr((const char *)wflanding_start, "{V}");
    char *lChar = strstr((const char *)wflanding_start, "{L}");
    ESP_LOGI(TAG, "Set-up smart Wi-Fi sensor");
    httpd_resp_set_type(req, "text/html");

    httpd_resp_send_chunk(req, wflanding_start, cChar - wflanding_start);
    httpd_resp_sendstr_chunk(req, (char *)s_countdown);
    httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
    if (!langVI)
    {
        httpd_resp_send_chunk(req, eChar + 3, aChar - (eChar + 3));
        if (errorIndex == ERROR_INVALID_PIN)
        {
            httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN);
            errorIndex = 0;
        }
        else
        {
            if (errorIndex == ERROR_INVALID_SSID)
            {
                httpd_resp_sendstr_chunk(req, NOTI_INVALID_SSID);
                errorIndex = 0;
            }
        }
        if (errorIndex == ERROR_WRONG_PIN)
        {
            httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
            errorIndex = 0;
        }
        httpd_resp_send_chunk(req, aChar + 3, vChar - (aChar + 3));
        httpd_resp_send_chunk(req, NULL, 0);
    }
    else
    {
        httpd_resp_send_chunk(req, vChar + 3, lChar - (vChar + 3));
        if (errorIndex == ERROR_INVALID_PIN)
        {
            httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN_VI);
            errorIndex = 0;
        }
        else
        {
            if (errorIndex == ERROR_INVALID_SSID)
            {
                httpd_resp_sendstr_chunk(req, NOTI_INVALID_SSID_VI);
                errorIndex = 0;
            }
        }
        if (errorIndex == ERROR_WRONG_PIN)
        {
            httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
            errorIndex = 0;
        }
        httpd_resp_send_chunk(req, lChar + 3, wflanding_end - (lChar + 3));
        httpd_resp_send_chunk(req, NULL, 0);
    }

    return ESP_OK;
}

static const httpd_uri_t WFLanding = {
    .uri = "/changeWFLanding",
    .method = HTTP_GET,
    .handler = WFLanding_handler};

// Confirm change Wi-Fi - EN
static esp_err_t changeWF_handler(httpd_req_t *req)
{
    uint8_t buffer[200];
    char ssid_r[32], password_r[64], _pinInput[5];
    httpd_req_recv(req, (char *)buffer, 200);
    if (httpd_query_key_value((char *)buffer, "pin", (char *)_pinInput, 5) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input PIN");
    }
    if (httpd_query_key_value((char *)buffer, "ssid", (char *)ssid_r, 32) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input SSID");
    }
    if (httpd_query_key_value((char *)buffer, "password", (char *)password_r, 64) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input Password");
    }

    ESP_LOGI(TAG, "PIN: %s, SSID: %s, Pass: %s", _pinInput, ssid_r, password_r);

    if (strcmp((char *)_pinInput, (char *)pinNumber) == 0 && strlen((char *)ssid_r) > 0)
    {
        strcpy((char *)sta_config.sta.ssid, (const char *)ssid_r);
        if (strlen((char *)password_r) >= 8)
        {
            strcpy((char *)sta_config.sta.password, (const char *)password_r);
        }
        else
        {
            strcpy((char *)sta_config.sta.password, "");
        }
        writeNVS(0);
        writeNVS(1);
        haveChange = true;

        char *cChar = strstr((const char *)wf_start, "{C}");
        char *eChar = strstr((const char *)wf_start, "{E}");
        char *sChar = strstr((const char *)wf_start, "{S}");
        char *pChar = strstr((const char *)wf_start, "{P}");
        char *vChar = strstr((const char *)wf_start, "{V}");
        char *tChar = strstr((const char *)wf_start, "{T}");
        char *mChar = strstr((const char *)wf_start, "{M}");

        ESP_LOGI(TAG, "Change Wi-Fi");
        httpd_resp_set_type(req, "text/html");
        httpd_resp_send_chunk(req, wf_start, cChar - wf_start);
        httpd_resp_sendstr_chunk(req, (char *)s_countdown);
        httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
        if (!langVI)
        {
            httpd_resp_send_chunk(req, eChar + 3, sChar - (eChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)ssid_r);
            httpd_resp_send_chunk(req, sChar + 3, pChar - (sChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)password_r);
            httpd_resp_send_chunk(req, pChar + 3, vChar - (pChar + 3));
            httpd_resp_send_chunk(req, NULL, 0);
        }
        else
        {
            httpd_resp_send_chunk(req, vChar + 3, tChar - (vChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)ssid_r);
            httpd_resp_send_chunk(req, tChar + 3, mChar - (tChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)password_r);
            httpd_resp_send_chunk(req, mChar + 3, wf_end - (mChar + 3));
            httpd_resp_send_chunk(req, NULL, 0);
        }
    }
    else
    {
        if (strlen((char *)ssid_r) == 0)
        {
            errorIndex = ERROR_INVALID_SSID;
        }
        if (strlen((char *)_pinInput) == 0)
            errorIndex = ERROR_INVALID_PIN;
        else if (strcmp((char *)_pinInput, (char *)pinNumber) != 0)
        {
            errorIndex = ERROR_WRONG_PIN;
            n_count_wrong_pin++;

            if (!langVI)
            {
                if (n_count_wrong_pin == 1)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d time", n_count_wrong_pin);
                else if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d times", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Enter the wrong PIN more than 6 times<br>Disconnect after <span id='e'></span> seconds");
                }
            }
            else
            {
                if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Nhập sai mã PIN %d lần", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Nhập sai mã PIN quá 6 lần<br>Ngắt kết nối sau <span id='e'></span> giây");
                }
            }
        }
        ESP_LOGI(TAG, "Not change Wi-Fi");
        WFLanding_handler(req);
    }

    return ESP_OK;
}

static const httpd_uri_t changeWF = {
    .uri = "/changeWf",
    .method = HTTP_POST,
    .handler = changeWF_handler};

// Landingpage change PIN - EN
static esp_err_t PINLanding_handler(httpd_req_t *req)
{
    // const uint32_t pinlanding_len = pinlanding_end - pinlanding_start;
    char *cChar = strstr((const char *)pinlanding_start, "{C}");
    char *eChar = strstr((const char *)pinlanding_start, "{E}");
    char *aChar = strstr((const char *)pinlanding_start, "{A}");
    char *vChar = strstr((const char *)pinlanding_start, "{V}");
    char *lChar = strstr((const char *)pinlanding_start, "{L}");

    ESP_LOGI(TAG, "Change new PIN");
    httpd_resp_set_type(req, "text/html");

    httpd_resp_send_chunk(req, pinlanding_start, cChar - pinlanding_start);
    httpd_resp_sendstr_chunk(req, (char *)s_countdown);
    httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
    ESP_LOGI(TAG, "Language VI: %d", langVI);
    if (!langVI)
    {
        httpd_resp_send_chunk(req, eChar + 3, aChar - (eChar + 3));
        if (errorIndex == ERROR_INVALID_PIN)
        {
            httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN);
            errorIndex = 0;
        }
        else
        {
            if (errorIndex == ERROR_INVALID_N_PIN)
            {
                httpd_resp_sendstr_chunk(req, NOTI_INVALID_N_PIN);
                errorIndex = 0;
            }
            else if (errorIndex == ERROR_PIN_NOMATCH)
            {
                httpd_resp_sendstr_chunk(req, NOTI_PIN_NOMATCH);
                errorIndex = 0;
            }
        }
        if (errorIndex == ERROR_WRONG_PIN)
        {
            httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
            errorIndex = 0;
        }
        httpd_resp_send_chunk(req, aChar + 3, vChar - (aChar + 3));
        httpd_resp_send_chunk(req, NULL, 0);
    }
    else
    {
        httpd_resp_send_chunk(req, vChar + 3, lChar - (vChar + 3));
        if (errorIndex == ERROR_INVALID_PIN)
        {
            httpd_resp_sendstr_chunk(req, NOTI_INVALID_PIN_VI);
            errorIndex = 0;
        }
        else
        {
            if (errorIndex == ERROR_INVALID_N_PIN)
            {
                httpd_resp_sendstr_chunk(req, NOTI_INVALID_N_PIN_VI);
                errorIndex = 0;
            }
            else if (errorIndex == ERROR_PIN_NOMATCH)
            {
                httpd_resp_sendstr_chunk(req, NOTI_PIN_NOMATCH_VI);
                errorIndex = 0;
            }
        }
        if (errorIndex == ERROR_WRONG_PIN)
        {
            httpd_resp_sendstr_chunk(req, (char *)notify_wrong);
            errorIndex = 0;
        }
        httpd_resp_send_chunk(req, lChar + 3, pinlanding_end - (lChar + 3));
        httpd_resp_send_chunk(req, NULL, 0);
    }

    return ESP_OK;
}

static const httpd_uri_t PINLanding = {
    .uri = "/changePinLanding",
    .method = HTTP_GET,
    .handler = PINLanding_handler};

// Confirm change new PIN - EN
static esp_err_t changePIN_handler(httpd_req_t *req)
{
    // const uint32_t root_len = root_end - root_start;
    uint8_t buffer[50];
    char newPin[5], confirmPin[5], _pinInput[5];
    httpd_req_recv(req, (char *)buffer, 50);
    if (httpd_query_key_value((char *)buffer, "pin", (char *)_pinInput, 5) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input PIN");
    }
    if (httpd_query_key_value((char *)buffer, "newPin", (char *)newPin, 5) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input new PIN");
    }
    if (httpd_query_key_value((char *)buffer, "confirmPin", (char *)confirmPin, 5) == ESP_ERR_NOT_FOUND)
    {
        ESP_LOGI(TAG, "No input confirm PIN");
    }

    char *cChar = strstr((const char *)pin_start, "{C}");
    char *eChar = strstr((const char *)pin_start, "{E}");
    char *pChar = strstr((const char *)pin_start, "{P}");
    char *vChar = strstr((const char *)pin_start, "{V}");
    char *mChar = strstr((const char *)pin_start, "{M}");

    if (!langVI)
    {
        if (strcmp((char *)_pinInput, (char *)pinNumber) == 0 && strcmp((char *)newPin, (char *)confirmPin) == 0 && strlen((char *)newPin) > 0)
        {
            strcpy((char *)pinNumber, (char *)newPin);
            writeNVS(2);

            ESP_LOGI(TAG, "Change PIN");
            httpd_resp_set_type(req, "text/html");
            httpd_resp_send_chunk(req, pin_start, cChar - pin_start);
            httpd_resp_sendstr_chunk(req, (char *)s_countdown);
            httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
            httpd_resp_send_chunk(req, eChar + 3, pChar - (eChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)newPin);
            httpd_resp_send_chunk(req, pChar + 3, vChar - (pChar + 3));
            httpd_resp_send_chunk(req, NULL, 0);
        }
        else
        {
            if (strlen((char *)newPin) == 0)
            {
                errorIndex = ERROR_INVALID_N_PIN;
            }
            else if (strcmp((char *)newPin, (char *)confirmPin) != 0)
            {
                errorIndex = ERROR_PIN_NOMATCH;
            }
            if (strlen((char *)_pinInput) == 0)
                errorIndex = ERROR_INVALID_PIN;
            else if (strcmp((char *)_pinInput, (char *)pinNumber) != 0)
            {
                errorIndex = ERROR_WRONG_PIN;
                n_count_wrong_pin++;

                if (n_count_wrong_pin == 1)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d time", n_count_wrong_pin);
                else if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Enter the wrong PIN %d times", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Enter the wrong PIN more than 6 times<br>Disconnect after <span id='e'></span> seconds");
                }
            }
            ESP_LOGI(TAG, "Not change PIN");
            PINLanding_handler(req);
        }
    }
    else
    {
        if (strcmp((char *)_pinInput, (char *)pinNumber) == 0 && strcmp((char *)newPin, (char *)confirmPin) == 0 && strlen((char *)newPin) > 0)
        {
            strcpy((char *)pinNumber, (char *)newPin);
            writeNVS(2);

            ESP_LOGI(TAG, "Change PIN");
            httpd_resp_set_type(req, "text/html");
            httpd_resp_send_chunk(req, pin_start, cChar - pin_start);
            httpd_resp_sendstr_chunk(req, (char *)s_countdown);
            httpd_resp_send_chunk(req, cChar + 3, eChar - (cChar + 3));
            httpd_resp_send_chunk(req, vChar + 3, mChar - (vChar + 3));
            httpd_resp_sendstr_chunk(req, (char *)newPin);
            httpd_resp_send_chunk(req, mChar + 3, pin_end - (mChar + 3));
            httpd_resp_send_chunk(req, NULL, 0);
        }
        else
        {
            if (strlen((char *)newPin) == 0)
            {
                errorIndex = ERROR_INVALID_N_PIN;
            }
            else if (strcmp((char *)newPin, (char *)confirmPin) != 0)
            {
                errorIndex = ERROR_PIN_NOMATCH;
            }
            if (strlen((char *)_pinInput) == 0)
                errorIndex = ERROR_INVALID_PIN;
            else if (strcmp((char *)_pinInput, (char *)pinNumber) != 0)
            {
                errorIndex = ERROR_WRONG_PIN;
                n_count_wrong_pin++;

                if (n_count_wrong_pin <= 6)
                    sprintf((char *)notify_wrong, "Nhập sai mã PIN %d lần", n_count_wrong_pin);
                else
                {
                    n_countdown = 3;
                    sprintf((char *)s_countdown, "%d", n_countdown);
                    strcpy((char *)notify_wrong, "Nhập sai mã PIN quá 6 lần<br>Ngắt kết nối sau <span id = 'e'></span> giây");
                }
            }
            ESP_LOGI(TAG, "Not change PIN");
            PINLanding_handler(req);
        }
    }

    return ESP_OK;
}
static const httpd_uri_t changePIN = {
    .uri = "/changePin",
    .method = HTTP_POST,
    .handler = changePIN_handler};

// HTTP Error (404) Handler - Redirects all requests to the root page
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    // Set status
    httpd_resp_set_status(req, "302 Temporary Redirect");
    // Redirect to the "/" root directory
    httpd_resp_set_hdr(req, "Location", "/");
    // iOS requires content in the response to detect a captive portal, simply redirecting is not sufficient.
    httpd_resp_send(req, "Redirect to the captive portal", HTTPD_RESP_USE_STRLEN);

    ESP_LOGI(TAG, "Redirecting to root");
    return ESP_OK;
}

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.max_open_sockets = 13;
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &root);
        httpd_register_uri_handler(server, &unlock);
        httpd_register_uri_handler(server, &WFLanding);
        httpd_register_uri_handler(server, &changeWF);
        httpd_register_uri_handler(server, &PINLanding);
        httpd_register_uri_handler(server, &changePIN);
        httpd_register_uri_handler(server, &root_vi);
        httpd_register_uri_handler(server, &lang_en);
        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    return server;
}

bool readNVS()
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return false;
    }
    else
    {
        printf("Done!\n");

        size_t str3Size = 5;
        err = nvs_get_str(my_handle, "pinNumber", (char *)pinNumber, &str3Size);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("pinNumber = %s\n", pinNumber);
            break;
        default:
            break;
        }

        size_t str1Size = 32;
        err = nvs_get_str(my_handle, "SSID", (char *)sta_config.sta.ssid, &str1Size);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("SSID = %s\n", sta_config.sta.ssid);
            break;
        default:
            return false;
        }

        size_t str2Size = 64;
        err = nvs_get_str(my_handle, "PWD", (char *)sta_config.sta.password, &str2Size);
        if (err == ESP_OK)
        {
            printf("Done\n");
            printf("Password = %s\n", sta_config.sta.password);
        }
    }
    nvs_close(my_handle);
    return true;
}

bool writeNVS(uint8_t key)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);

    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        switch (key)
        {
        case 0:
            err = nvs_set_str(my_handle, "SSID", (char *)sta_config.sta.ssid);
            ESP_LOGI(TAG, "Save new SSID: %s", (char *)sta_config.sta.ssid);
            break;
        case 1:
            err = nvs_set_str(my_handle, "PWD", (char *)sta_config.sta.password);
            ESP_LOGI(TAG, "Save new PASS: %s", (char *)sta_config.sta.password);
            break;
        case 2:
            err = nvs_set_str(my_handle, "pinNumber", (char *)pinNumber);
            ESP_LOGI(TAG, "Save new PIN: %s", (char *)pinNumber);
            break;
        default:
            break;
        }
        if (err != ESP_OK)
            return false;
        else
        {
            err = nvs_commit(my_handle);

            if (err != ESP_OK)
                return false;
        }
    }
    nvs_close(my_handle);
    return true;
}

void sysAction(void *pvParameters)
{
    const TickType_t tDelay = 10 / portTICK_PERIOD_MS;
    while (1)
    {
        if (systemActive)
        {
            gpio_set_level(ACTION_PIN, true);
        }
        else
        {
            gpio_set_level(ACTION_PIN, false);
        }
        vTaskDelay(tDelay);
    }
    vTaskDelete(NULL);
}

void countdown(void *pvParameters)
{
    const TickType_t tDelay = 1000 / portTICK_PERIOD_MS;
    while (1)
    {
        if ((softApOn && n_countdown == 0) || n_count_wrong_pin > 6)
        {
            esp_wifi_stop();
            if (smartSensor)
                wifi_init_sta();
            softApOn = false;
            n_count_wrong_pin = 0;
        }
        if (n_countdown > 0)
        {
            n_countdown--;
            sprintf((char *)s_countdown, "%d", n_countdown);
            ESP_LOGI(TAG, "%s", (char *)s_countdown);
        }
        vTaskDelay(tDelay);
    }
    vTaskDelete(NULL);
}

void app_main(void)
{
    gpio_reset_pin(ACTION_PIN);
    gpio_set_direction(ACTION_PIN, GPIO_MODE_OUTPUT);
    /*
        Turn of warnings from HTTP server as redirecting traffic will yield
        lots of invalid requests
    */
    esp_log_level_set("httpd_uri", ESP_LOG_ERROR);
    esp_log_level_set("httpd_txrx", ESP_LOG_ERROR);
    esp_log_level_set("httpd_parse", ESP_LOG_ERROR);

    // Initialize networking stack
    ESP_ERROR_CHECK(esp_netif_init());

    // Create default event loop needed by the  main app
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // Initialize NVS needed by Wi-Fi
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    if (readNVS())
    {
        if (strlen((char *)sta_config.sta.ssid) > 0)
            smartSensor = true;
    }

    // Initialise ESP32 in SoftAP mode
    wifi_init_softap();
    start_webserver();
    start_dns_server();

    xTaskCreate(sysAction, "unlock", 4096, NULL, 5, NULL);
    xTaskCreate(countdown, "countdownAP", 4096, NULL, 5, NULL);
}
